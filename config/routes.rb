Rails.application.routes.draw do
  resources :departamentos
  resources :produtos, only: [:new, :create, :destroy, :edit, :update]
  get "/produtos/busca" => "produtos/busca", as: :busca_produto
  root "produtos#index"


  # post "/produtos" => "produtos#create"
  # get "/produtos/new" => "produtos#new"  
  # delete "/produtos/:id" => "produtos#destroy", as: :produto
  # get "produtos" => "produtos#index"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  # get "[o nome que queremos para a URL]" => "[nome do Controller]#[nome da página]"
end
